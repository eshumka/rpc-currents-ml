The repository for the ML-based tool for CMS RPC currents quality monitoring. Intended to be linked with and deployed on the CERN PaaS, which is based on the newest version of the community edition of OpenShift, OKD4.

Deployed on OpenShift in October 2022.
