FROM python:3.6

ENV PYTHONUNBUFFERED=1

COPY . /tmp/code/
COPY requirements.txt /tmp/code/requirements.txt

RUN pip install --upgrade pip \ 
 && pip install --upgrade pipenv \
 && pip install -r /tmp/code/requirements.txt

EXPOSE 8080

WORKDIR /tmp/code/

ENV PYTHONPATH /tmp/code

CMD ["python", "uiv2/uiv2.py", "8080"]
